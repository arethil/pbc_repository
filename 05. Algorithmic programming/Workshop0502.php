<?php
    /*
    Задача:
    С некоторого момента прошло 234 дня.
    Сколько полных недель прошло за этот период?
    А месяцев?
    */
    
    $days = 234;
    // $weeks = $days / 7;
    // $months = $days / 30;
    // echo sprintf("Days: %d\nWeeks: %d\nMonths: %d\n", $days, $weeks, $months);
    echo sprintf("Days: %d\nWeeks: %d\nMonths: %d\n", $days, $days / 7, $days / 30);
?>
