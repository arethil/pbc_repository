public class Homework0501
{
    public static void main(String []args)
    {
        // Вывести на экран число «Пи» с точностью до сотых.
        /*
        double pi = Math.PI;
        System.out.printf("%.2f \n", pi);
        */

        // Вывести на экран числа 50 и 10 одно под другим.
        
        // System.out.println("50\n10");
        
        /*
        Даны два целых числа 4 и 5. Найти:
        а) их среднее арифметическое;
        б) их среднее геометрическое.
        */
        /*
        int x = 4;
        int y = 5;
        double aAvg = (double)(x + y) / 2;
        double gAvg = Math.sqrt(x * y);
        System.out.println("Arithmetic average: " + aAvg);
        System.out.println("Geometric average: " + gAvg);        
        */

        /*
        Составить программу обмена значениями трёх переменных величин а = 2, b = 3, c = 1
        по следующей схеме:
        а) b присвоить значение c, а присвоить значение b, с присвоить значение а;
        б) b присвоить значение а, с присвоить значение b, а присвоить значение с.
        */

        int a = 2, b = 3, c = 1;
        int d = b;
        System.out.printf("a = %d; b = %d; c = %d \n", a, b, c);
        /*
        b = c;
        c = a;
        a = d;
        System.out.printf("a = %d; b = %d; c = %d \n", a, b, c);
        */
        b = a;
        a = c;
        c = d;
        System.out.printf("a = %d; b = %d; c = %d \n", a, b, c);
    }
}
