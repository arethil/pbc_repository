public class Classwork0501
{
    public static void main(String []args)
    {
        // Вывод сообщений
        System.out.println("Hello, World!");
        System.out.println("Welcome to Java!");
        /*
        Многострочный комментарий
        Вывод нескольких сообщений
        */
        
        int x, y = 25;
        x = 10;
        System.out.println(x);
        System.out.println(y);        
        
        y = x;
        System.out.println(x);
        System.out.println(y);
                
        boolean x1 = false;
        byte x2 = 1;
        short x3 = 2;
        int x4 = 4;
        long x5 = 3147886542l;
        double x6 = 8.5;
        float x7 = 8.5f;
        char x8 = 102;
        char x9 = 'f';
        String x10 = "Text";

        System.out.println(x1);
        System.out.println(x2);
        System.out.println(x3);
        System.out.println(x4);
        System.out.println(x5);
        System.out.println(x6);
        System.out.println(x7);
        System.out.println(x8);
        System.out.println(x9);
        System.out.println(x10);
                
        final double g = 9.81;
        double z = g;
        System.out.println(g);
        System.out.println(z);
                
        int y1 = 20;
        double y2 = 12;

        System.out.println(y1 + 5);     // 25
        System.out.println(y1);         // 20
        System.out.println(y1 - 5);     // 15
        System.out.println(y1 * 5);     // 100
        System.out.println(y1 / 5);     // 4
        System.out.println(y2 / 5);     // 2.4
        System.out.println(y1 % 6);     // 2
        System.out.println(y1);         // 20
        System.out.println(y1 += 5);    // 25
        System.out.println(y1);         // 25
        System.out.println(y1 -= 5);    // 20
        System.out.println(y1 *= 5);    // 100
        System.out.println(y1 /= 5);    // 20
        System.out.println(y1 %= 6);    // 2
        System.out.println(y1);         // 2
        System.out.println(y1++);       // 2
        System.out.println(y1);         // 3
        System.out.println(++y1);       // 4
        System.out.println(y1);         // 4
        System.out.println(y1--);       // 4
        System.out.println(y1);         // 3
        System.out.println(--y1);       // 2
        System.out.println(y1);         // 2
                
        int i1 = 4;
        // byte b1 = i1;
        byte b1 = (byte)i1;
        System.out.println(b1);
        
        int i2 = b1;
        System.out.println(i2);
        
        int i3 = 2_147_483_647;
        float f = i3;
        System.out.println(f);
        
        long l = 8l;
        int i4 = (int)l;
        System.out.println(i4);
        
        int i5 = 258;
        byte b2 = (byte)i5;
        System.out.println(b2);
        
        double d1 = 56.9898;
        int i6 = (int)d1;
        System.out.println(i6);
        
        int i7 = 4;
        double d2 = 3.6;
        double d3 = i7 + d2;
        System.out.println(d3);
        
        byte b3 = 1;
        short s = 2;
        byte b4 = (byte)(b3 + s);
        System.out.println(b4);
        
        int i8 = 'a' + 5;
        System.out.println(i8);
                
        System.out.println("\'Text\'");
        System.out.println("\"Text\"");
        System.out.println("\\Text\\");
        System.out.print("Text \n");
        System.out.println("\t Text");
        
        int a = 5;
        int b = 6;
        System.out.println("a = " + a + "; b = " + b);
        System.out.printf("a = %d; b = %d \n", a, b);
        
        String name = "Alexander";
        int age = 35;
        double height = 1.8;
        System.out.printf("Name: %s; Age: %d; Height: %.2f \n", name, age, height);
                
        double pi = Math.PI;
        System.out.println(pi);
        
        double e = Math.E;
        System.out.println(e);
        
        double sin = Math.sin(0);
        System.out.println(sin);
        
        double cos = Math.cos(0);
        System.out.println(cos);
        
        double tan = Math.tan(0);
        System.out.println(tan);
        
        double c = Math.sqrt(16);
        System.out.println(c);
        
        double d = Math.pow(3, 2);
        System.out.println(d);
    }
}
