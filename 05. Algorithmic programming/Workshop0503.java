public class Workshop0503
{
    public static void main(String []args)
    {
        /*
        Задача:
        Дано двузначное число 39.
        Найти:
        а) число десятков в нём;
        б) число единиц в нём;
        в) сумму его цифр;
        г) произведение его цифр.
        */
        
        int num = 39;
        int t = num / 10;
        int u = num % 10;
        int sum = t + u;
        int pro = t * u;
        System.out.printf("Tens: %d\nUnits: %d\nSum: %d\nProduct: %d\n", t, u, sum, pro);
    }
}
