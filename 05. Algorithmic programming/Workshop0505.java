public class Workshop0505
{
    public static void main(String []args)
    {
        /*
        Задача:
        Известно значение температуры по шкале Цельсия 36,6.
        Найти соответствующее значение температуры по шкале:
        а) Фаренгейта;
        б) Кельвина.
        Для пересчета по шкале Фаренгейта необходимо
        исходное значение температуры умножить на 1,8 и к
        результату прибавить 32, а по шкале Кельвина
        абсолютное значение нуля соответствует 273,15
        градуса по шкале Цельсия.
        */
        
        double c = 36.6;
        double f = (c * 1.8) + 32;
        double k = c + 273.15;
        System.out.printf("Celsius: %.2f\nFahrenheit: %.2f\nKelvin: %.2f\n", c, f, k);
    }
}
