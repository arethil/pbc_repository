public class Workshop0501
{
    public static void main(String []args)
    {
        /*
        Задача:
        Расстояние в сантиметрах равно 48.
        Найти число полных метров в нём.
        */
        
        int x = 48;
        int y = x / 100;
        System.out.println("Number of complete meters: " + y);
    }
}
