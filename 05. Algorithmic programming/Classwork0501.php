<?php
    // Вывод сообщений
    echo "Hello, World! \n";
    echo "Welcome to PHP! \n";
    /*
    Многострочный комментарий
    Вывод нескольких сообщений
    */
        
    $x; $y = 25;
    $x = 10;
    echo $x, "\n";
    echo $y, "\n";    
    
    $y = $x;
    echo $x, "\n";
    echo $y, "\n";
        
    $x1 = 4;
    $x2 = 8.5;
    $x3 = false;
    $x4 = "Text";
    
    echo $x1, "\n";
    echo $x2, "\n";
    echo var_export($x3), "\n";
    echo $x4, "\n";
        
    const g = 9.81;
    $z = g;
    echo g, "\n";
    echo $z, "\n";
        
    $y1 = 20;

    echo $y1 + 5, "\n";     // 25
    echo $y1, "\n";         // 20
    echo $y1 - 5, "\n";     // 15
    echo $y1 * 5, "\n";     // 100
    echo $y1 / 5, "\n";     // 4
    echo $y1 % 6, "\n";     // 2
    echo $y1, "\n";         // 20
    echo $y1 += 5, "\n";    // 25
    echo $y1, "\n";         // 25
    echo $y1 -= 5, "\n";    // 20
    echo $y1 *= 5, "\n";    // 100
    echo $y1 /= 5, "\n";    // 20
    echo $y1 %= 6, "\n";    // 2
    echo $y1, "\n";         // 2
    echo $y1++, "\n";       // 2
    echo $y1, "\n";         // 3
    echo ++$y1, "\n";       // 4
    echo $y1, "\n";         // 4
    echo $y1--, "\n";       // 4
    echo $y1, "\n";         // 3
    echo --$y1, "\n";       // 2
    echo $y1, "\n";         // 2
        
    $d1 = 56.9898;
    $i1 = $d1;
    echo $i1, "\n";         // 56.9898
    settype($i1, "integer");
    echo $i1, "\n";         // 56
    
    $i2 = 4;
    $d2 = 8.6;
    $d3 = $i2 + $d2;
    echo $d3, "\n";         // 12.6
    /*
    $i3 = "a" + 5;          // Ошибка!
    echo $i3, "\n";
    */
        
    echo "'Text' \n";
    echo "\"Text\" \n";
    echo "\\Text\\ \n";
    echo "Text \n";
    echo "\t Text \n";
    
    $a = 5;
    $b = 6;
    echo "a = " . $a . "; b = " . $b, "\n";
    echo sprintf("a = %d; b = %d \n", $a, $b);
    
    $name = "Alexander";
    $age = 35;
    $height = 1.8;
    echo sprintf("Name: %s; Age: %d; Height: %.2f \n", $name, $age, $height);
        
    $pi = pi();
    echo $pi, "\n";
    
    $e = exp(1);
    echo $e, "\n";
    
    $m1 = sin(0);
    echo $m1, "\n";
    
    $m2 = cos(0);
    echo $m2, "\n";
    
    $m3 = tan(0);
    echo $m3, "\n";
    
    $m4 = sqrt(16);
    echo $m4, "\n";
    
    $m5 = pow(2, 3);
    echo $m5, "\n";
?>
