<?php
    class cNode
    {
        public $next;
        public $data;
        
        public function __construct($data)
        {
            $this->data = $data;
        }
    }
    
    class cStack
    {
        private $top;
        
        public function push($node)
        {
            if($node == null)
            {
                return;
            }
            else
            {
                $n = $node;
                $n->next = $this->top;
                $this->top = $n;
            }
        }
        
        public function pop()
        {
            if($this->top == null)
            {
                return;
            }
            else
            {
                $proxyTop = $this->top;
                $this->top = $this->top->next;
                return $proxyTop;
            }
        }
        
        public function printStack()
        {
            echo "Stack consists of: ";
            $n = $this->top;

            while($n != null)
            {
                echo $n->data . " ";
                $n = $n->next;
            }

            echo "\n";
        }
    }
    
    $s = new cStack();

    for($i = 1; $i < 10; $i++)
        $s->push(new cNode($i));

    $s->printStack();
    
    for($i = 1; $i < 5; $i++)
    {
        $n = $s->pop();
        echo $n->data, "\n";
    }

    $s->printStack();
    
    class cQueue
    {
        private $first;
        private $last;
        
        public function push($node)
        {
            if($node == null)
            {
                return;
            }
            else
            {
                $n = $node;
                
                if($this->first == null)
                {
                    $this->last = $n;
                    $this->first = $this->last;
                }
                else
                {
                    $this->last->next = $n;
                    $this->last = $this->last->next;
                }
            }
        }
        
        public function pop()
        {
            if($this->first == null)
            {
                return;
            }
            else
            {
                $buf = $this->first;

                if($this->first == $this->last)
                {                    
                    $this->first = null;
                    $this->last = null;
                    return $buf;
                }
                else
                {
                    $this->first = $this->first->next;
                    return $buf;
                }
            }
        }
        
        public function printQueue()
        {
            echo "Queue consists of: ";
            $n = $this->first;

            while($n != null)
            {
                echo $n->data . " ";
                $n = $n->next;
            }

            echo "\n";
        }
    }
    
    $q = new cQueue();

    for($i = 1; $i < 10; $i++)
        $q->push(new cNode($i));

    $q->printQueue();
    
    for($i = 1; $i < 5; $i++)
    {
        $n = $q->pop();
        echo $n->data, "\n";
    }
    
    $q->printQueue();
?>
