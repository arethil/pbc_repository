<?php
    abstract class Animal
    {
        abstract protected function makeSound();
        
        public function thisIsAnAnimal()
        {
            return " is an animal \n";
        }
    }
    
    interface CallPet
    {
        public function setName($newName);

        public function call();
    }
    
    abstract class Pet extends Animal implements CallPet
    {
        protected $name;
        public $hasFleas;
        
        public function setName($newName)
        {
            $this->name = $newName;
        }
        
        public function call()
        {
            return $this->name . " said " . $this->makeSound() . "\n";
        }
        
        function __construct($name, $hasFleas)
        {
            $this->setName($name);
            $this->hasFleas = $hasFleas;
        }
        
        public function thisIsAnAnimal()
        {
            return $this->name . parent::thisIsAnAnimal();
        }
        
        public function isFleaBitten()
        {
            if($this->hasFleas)
                return $this->name . " has fleas! \n";
            else
                return $this->name . " has not fleas \n";
        }
        
        public function getUpWithFleas($pet)
        {
            $this->hasFleas = $pet->hasFleas;
            echo $this->name . " contacted with " . $pet->name . "\n";
        }
    }
    
    class Dog extends Pet
    {
        function __construct($name, $hasFleas)
        {
            parent::__construct($name, $hasFleas);
        }
        
        protected function makeSound()
        {
            return "Woof!";
        }
        
        public function giveAPaw()
        {
            echo $this->name . " gave a paw! \n";
        }
    }
    
    class Cat extends Pet
    {
        function __construct($name, $hasFleas)
        {
            parent::__construct($name, $hasFleas);
        }
        
        protected function makeSound()
        {
            return "Meow!";
        }
    }
    
    class Person
    {
        public $name;
        public $pet;
        
        function __construct($name, $pet)
        {
            $this->name = $name;
            echo $this->name . " wants to take care on pet \n";
            $this->pet = $pet;
            echo $this->pet->call();
        }
        
        public function treatForFleas()
        {
            echo $this->pet->isFleaBitten();
            $this->pet->hasFleas = false;
            echo $this->name . " treats for fleas \n";
            echo $this->pet->isFleaBitten();
        }
    }
    
    $c = new Cat("Mufin", false);
    $d = new Dog("Nord", true);
    
    echo $c->thisIsAnAnimal();
    echo $d->thisIsAnAnimal();
    
    echo $c->call();
    echo $d->call();
    
    $d->giveAPaw();
    
    echo $c->isFleaBitten();
    echo $d->isFleaBitten();
    
    $c->getUpWithFleas($d);
    echo $c->isFleaBitten();
    
    $p = new Person("Nataly", $c);
    $p->treatForFleas();
?>
