public class Workshop060211
{
    public static void main(String []args)
    {
        /*
        Задача:
        Найти максимальное из натуральных чисел,
        не превышающих 5000, которое нацело делится на 39.
        */
        
        int num = 5000;
        
        do
        {
            num--;
        }
        while(num % 39 != 0);
        
        System.out.println(num);
    }
}
