public class Workshop060207
{
    public static void main(String []args)
    {
        /*
        Задача:
        Напечатать таблицу соответствия между
        весом в фунтах и весом в килограммах для значений
        1, 2, ..., 10 фунтов (1 фунт = 453 г).
        */
        
        for(int f = 1; f <= 10; f++)
        {
            System.out.println(f + " lb = " + f * 453 + " grams");
        }
    }
}
