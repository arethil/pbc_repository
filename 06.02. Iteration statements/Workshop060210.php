<?php
    /*
    Задача:
    Дано натуральное число. Определить:
    а) количество цифр 3 в нём;
    б) сколько раз в нём встречается последняя цифра;
    в) количество чётных цифр в нём. Составное условие
    и более одного неполного условного оператора не использовать;
    г) сумму его цифр, больших пяти;
    д) произведение его цифр, больших семи;
    е) сколько раз в нём встречаются цифры 0 и 5 (всего).
    */
    
    $in = fopen('php://stdin', "r");
    
    fscanf($in, "%d", $num);
    
    $count1 = 0;
    $count2 = 0;
    $count3 = 0;
    $count4 = 0;
    
    $last = $num % 10;
    $sum = 0;
    $pro = 1;
    
    while($num != 0)
    {
        $temp = $num % 10;
        $num /= 10;
        settype($num, "integer");
        echo $temp, "\t", $num, "\n";
        
        if($temp == 3)
            $count1++;
        
        if($temp == $last)
            $count2++;
        
        if(($temp % 2) == 0)
            $count3++;
        
        if($temp > 5)
            $sum += $temp;
        
        if($temp > 7)
            $pro *= $temp;
        
        if(($temp == 0) || ($temp == 5))
            $count4++;
    }
    
    if($pro == 1)
        $pro = 0;
        
    echo "Number of digits 3: " . $count1, "\n";
    echo "Number of last digits: " . $count2, "\n";
    echo "Number of even digits: " . $count3, "\n";
    echo "Sum of numbers greater than 5: " . $sum, "\n";
    echo "Product of numbers greater than 7: " . $pro, "\n";
    echo "Number of digits 0 or 5: " . $count4, "\n";

    fclose($in);
?>
