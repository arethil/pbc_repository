import java.util.Scanner;

public class Workshop060205
{
    public static void main(String []args)
    {
        /*
        Задача:
        Даны натуральное число n и вещественные числа a1, a2 ... an.
        Определить среднее арифметическое вещественных чисел.
        */

        Scanner in = new Scanner(System.in);
        
        int n = in.nextInt();        
        
        if(n > 0)
        {
            double sum = 0;
            
            for(int i = 0; i < n; i++)
            {
                double num = in.nextDouble();
                sum += num;
            }

            double aAvg = sum / n;
            System.out.println(aAvg);
        }
        else
        {
            System.out.println("Incorrect value \"n\"");
        }
        
        in.close();
    }
}
