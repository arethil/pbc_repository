<?php
    /*
    Задача:
    Последовательность Фибоначчи образуется так:
    первый и второй члены последовательности равны 1,
    каждый следующий равен сумме двух предыдущих (1, 1, 2, 3, 5, 8, 13, ...).
    Дано натуральное число n.
    а) Найти k-й член последовательности Фибоначчи;
    б) Получить первые n членов последовательности Фибоначчи;
    в) Верно ли, что сумма первых n членов последовательности Фибоначчи есть чётное число?
    */
    
    $in = fopen('php://stdin', "r");

    fscanf($in, "%d", $k);
    fscanf($in, "%d", $n);
    
    if($k <= 0)
    {
        echo "Incorrect value \"k\" \n";
    }
    else
    {
        if(($k == 1) || ($k == 2))
        {
            echo 1, "\n";
        }
        else
        {
            $fib1 = 1;
            $fib2 = 1;
            $fib_k;
            
            for($i = 3; $i <= $k; $i++)
            {
                $fib_k = $fib1 + $fib2;
                $fib1 = $fib2;
                $fib2 = $fib_k;
            }

            echo sprintf("%d \n", $fib_k);
        }
    }
    
    if($n <= 0)
    {
        echo "Incorrect value \"n\" \n";
    }
    else
    {
        $fib1 = 1;
        $fib2 = 1;
        $sum = 0;
        $fib_n;
        
        for($i = 1; $i <= $n; $i++)
        {
            if(($i == 1) || ($i == 2))
            {
                $sum++;
                echo "1 ";
            }
            else
            {
                $fib_n = $fib1 + $fib2;                
                $sum += $fib_n;
                $fib1 = $fib2;
                $fib2 = $fib_n;
                echo sprintf("%d ", $fib_n);
            }
        }

        echo sprintf("\nSum: %d\n", $sum);
        
        $b = (($sum % 2) == 0);
        echo var_export($b), "\n";
    }
    
    fclose($in);
?>
