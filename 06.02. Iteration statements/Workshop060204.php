<?php
    /*
    Задача:
    Найти:
    а) сумму всех целых чисел от 100 до 500;
    б) сумму всех целых чисел от a до b (значения
    a и b вводятся с клавиатуры; b >= a).
    */

    // $a = 100;
    // $b = 500;
    
    $in = fopen('php://stdin', "r");

    fscanf($in, "%d", $a);
    fscanf($in, "%d", $b);
    
    $sum = 0;
    
    if($b < $a)
    {
        echo "Incorrect interval \n";
    }
    else
    {
        for($i = $a; $i <= $b; $i++)
        {
            $sum += $i;
        }
        
        echo $sum, "\n";
    }
    
    fclose($in);
?>
