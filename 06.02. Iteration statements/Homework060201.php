<?php
    /*
    Напечатать числа следующим образом:
    25 25.5 24.8
    26 26.5 25.8
    ...
    35 35.5 34.8
    */
    /*    
    for($i = 25; $i <= 35; $i++)
    {
        echo sprintf("%d %d.5 %d.8 \n", $i, $i, $i - 1);
    }
    */

    // Вычислить сумму 1+1/2+1/3+...+1/n.
    /*
    $in = fopen('php://stdin', "r");

    fscanf($in, "%d", $n);
    
    $sum = 0;
    
    if($n < 1)
    {
        echo "Incorrect value \"n\" \n";
    }
    else
    {
        for($i = 1; $i <= $n; $i++)
        {
            $sum += (1 / $i);
            // echo $sum, "\n";
        }
        
        echo "Sum of sequence numbers: " . $sum, "\n";
    }
    */
    
    /*
    Дано натуральное число.
    а) Определить его максимальную цифру;
    б) Определить его минимальную цифру.
    */
    $in = fopen('php://stdin', "r");
    
    fscanf($in, "%d", $num);
    
    if($num < 0)
    {
        echo "Incorrect number \n";
    }
    else
    {
        $last = $num % 10;
        $max = $last;
        $min = $last;
        
        while($num != 0)
        {
            $last = $num % 10;
            $num /= 10;
            settype($num, "integer");
            
            if($max < $last)
                $max = $last;
            
            if($min > $last)
                $min = $last;
            
            // echo sprintf("%d \t %d \n", $last, $num);
        }
        
        echo sprintf("Maximum digit: %d \n", $max);
        echo sprintf("Manimum digit: %d \n", $min);
    }
    
    fclose($in);
?>
