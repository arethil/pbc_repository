public class Classwork060201
{
    public static void main(String []args)
    {
        for(int i = 1; i < 10; i++)
        {
            System.out.println("Square of the number " + i + " is " + i * i);
        }
        
        int j = 6;

        while(j > 0)
        {
            System.out.println(j);
            j--;
        }
        
        int l = 1;

        do
        {
            System.out.println(l);
            l++;
        }
        while(l < 7);
        
        for(int i = 0; i <= 15; i++)
        {
            if(i > 10)
                break;

            System.out.println(i);
        }
        
        for(int i = 0; i <= 15; i++)
        {
            if((i > 8) && (i < 12))
                continue;
            
            System.out.println(i);
        }
    }
}
