import java.util.Scanner;

public class Workshop060209
{
    public static void main(String []args)
    {
        /*
        Задача:
        Дана непустая последовательность целых
        чисел, оканчивающаяся нулем. Найти:
        а) сумму всех чисел последовательности;
        б) количество всех чисел последовательности.
        */

        Scanner in = new Scanner(System.in);
        
        int num = in.nextInt();
        int count = 1;        
        int sum = 0;
        
        while(num != 0)
        {
            sum += num;
            System.out.println(num + "\t" + count + "\t" + sum);
            
            num = in.nextInt();
            count++;
        }
        
        System.out.println(num + "\t" + count + "\t" + sum);

        in.close();
    }
}
