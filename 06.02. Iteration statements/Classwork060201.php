<?php
    for($i = 1; $i < 10; $i++)
    {
        echo sprintf("Square of the number %d is %d \n", $i, $i * $i);
    }
    
    $j = 6;

    while($j > 0)
    {
        echo $j, "\n";
        $j--;
    }
    
    $l = 1;

    do
    {
        echo $l, "\n";
        $l++;
    }
    while($l < 7);
    
    for($i = 0; $i <= 15; $i++)
    {
        if($i > 10)
            break;

        echo $i, "\n";
    }
        
    for($i = 0; $i <= 15; $i++)
    {
        if(($i > 8) && ($i < 12))
            continue;
        
        echo $i, "\n";
    }
?>
