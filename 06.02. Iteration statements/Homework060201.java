import java.util.Scanner;

public class Homework060201
{
    public static void main(String []args)
    {
        // Составить программу вывода любого числа любое заданное число раз.
        /*
        Scanner in = new Scanner(System.in);

        int num = in.nextInt();
        int n = in.nextInt();
        
        if(n < 0)
        {
            System.out.println("Incorrect value \"n\"");
        }
        else
        {
            for(int i = 0; i < n; i++)
            {
                System.out.print(num + " ");
            }
        }
        
        in.close();
        */

        /*
        Напечатать таблицу умножения на 9:
        9 х 1 = 9
        9 х 2 = 18
        ...
        9 х 9 = 81
        */
        /*        
        for(int i = 1; i < 10; i++)
        {
            System.out.printf("9 x %d = %d \n", i, i * 9);
        }
        */

        // Даны числа a1, a2, … , a6. Определить их произведение.
        /*
        Scanner in = new Scanner(System.in);
        
        long pro = 1l;
        
        for(int i = 1; i <= 6; i++)
        {
            int num = in.nextInt();
            pro *= num;
            // System.out.println(pro);
        }
        
        System.out.println("Product of numbers: " + pro);

        in.close();
        */
        
        /*
        Имеется фрагмент программы в виде оператора цикла с параметром,
        обеспечивающий вывод на экран всех целых чисел от 100 до 80.
        Оформить этот фрагмент в виде:
        а) оператора цикла с предусловием;
        б) оператора цикла с постусловием.
        */
        for(int i = 100; i >= 80; i--)
        {
            System.out.print(i + " ");
        }
        
        System.out.println();
        
        int j = 100;

        while(j >= 80)
        {
            System.out.print(j + " ");
            j--;
        }
        
        System.out.println();
        
        int l = 100;

        do
        {
            System.out.print(l + " ");
            l--;
        }
        while(l >= 80);
    }
}
