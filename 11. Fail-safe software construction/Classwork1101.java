import java.util.Scanner;

public class Classwork1101
{
    public static void main(String []args)
    {
        /*
        int[] nums = new int[3];

        nums[4] = 37;
        */
        /*
        try
        {
            int[] nums = new int[3];

            nums[4] = 37;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            System.out.println("Program complited");
        }
        
        try
        {
            int[] nums = new int[3];

            // nums[4] = 4;
            nums[0] = Integer.parseInt("gfe");
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            System.out.println("!Array out of bounds!");
        }
        catch(NumberFormatException e)
        {
            System.out.println("!Error converting from string to number!");
        }
        */

        try
        {
            Scanner in = new Scanner(System.in);

            int x = in.nextInt();
            
            if(x > 30)
                throw new Exception("!The number x must be less than 30!");
            
            System.out.printf("x = %d \n", x);
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
