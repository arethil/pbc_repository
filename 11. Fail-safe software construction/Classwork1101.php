<?php
    function division($x, $y)
    {
        if($y === 0)
            throw new Exception("!Division by 0!");
        
        return $x / $y;
    }
    
    try
    {
        echo division(5, 3), "\n";
        echo division(5, 0), "\n";
        echo division(6, 4), "\n";
    }
    catch(Exception $e)
    {
        echo "Error: " . $e->getMessage(), "\n";
    }
    finally
    {
        echo "Program completed \n";
    }
?>
