public class Classwork060301
{
    public static void main(String []args)
    {
        // printNum(6);
        // printNum(maxNum(16, 9));
        
        int x = 3;
        int y = 7;
        System.out.printf("x = %d; y = %d \n", x, y);
        
        exchange(x, y);
        System.out.printf("x = %d; y = %d \n", x, y);
        
        int temp = x;
        x = y;
        y = temp;
        System.out.printf("x = %d; y = %d \n", x, y);
    }
    
    public static void printNum(int i)
    {
        System.out.println(i);
    }
    
    public static int maxNum(int a, int b)
    {
        if(a > b)
            return a;
        else
            return b;
    }
    
    public static void exchange(int a, int b)
    {
        int temp = a;
        a = b;
        b = temp;
    }
}
