<?php
    function printNum($i)
    {
        echo $i, "\n";
    }
    
    function maxNum($a, $b)
    {
        if($a > $b)
            return $a;
        else
            return $b;
    }
    
    // printNum(6);
    // printNum(maxNum(16, 9));
    
    function exchange1($a, $b)
    {
        $temp = $a;
        $a = $b;
        $b = $temp;
    }
    
    function exchange2(&$a, &$b)
    {
        $temp = $a;
        $a = $b;
        $b = $temp;
    }
    
    $x = 8;
    $y = 6;
    echo sprintf("x = %d; y = %d \n", $x, $y);
    
    exchange1($x, $y);
    echo sprintf("x = %d; y = %d \n", $x, $y);
    
    exchange2($x, $y);
    echo sprintf("x = %d; y = %d \n", $x, $y);
?>
