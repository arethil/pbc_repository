public class Workshop060301
{
    public static void main(String []args)
    {
        /*
        Задача:
        Напечатать числа в виде следующей таблицы:
        5 5 5 5 5 5
        5 5 5 5 5 5
        5 5 5 5 5 5
        5 5 5 5 5 5
        */
        
        printTable(5, 6, 4);
    }
    
    public static void printNum(int num)
    {
        System.out.print(num + " ");
    }
    
    public static void printRow(int num, int e)
    {
        for(int i = 1; i <= e; i++)
            printNum(num);
        
        System.out.println();
    }
    
    public static void printTable(int num, int columns, int rows)
    {
        for(int i = 1; i <= rows; i++)
            printRow(num, columns);
    }
}
