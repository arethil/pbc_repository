import java.util.Scanner;

public class Workshop060305
{
    public static void main(String []args)
    {
        /*
        Задача:
        Последовательность Фибоначчи образуется так:
        первый и второй члены последовательности равны 1,
        каждый следующий равен сумме двух предыдущих (1, 1, 2, 3, 5, 8, 13, ...).
        Дано натуральное число n.
        а) Найти k-й член последовательности Фибоначчи;
        б) Получить первые n членов последовательности Фибоначчи.
        Решить задачу с помощью рекурсивных функций.
        */
        
        Scanner in = new Scanner(System.in);
        
        int k = in.nextInt();
        int n = in.nextInt();
        
        if(k <= 0)
            System.out.println("Incorrect value \"k\"");
        else
            System.out.println(fib(k));
        
        if(n <= 0)
            System.out.println("Incorrect value \"n\"");
        else
            for(int i = 1; i <= n; i++)
                System.out.print(fib(i) + " ");
        
        in.close();
    }
    
    public static int fib(int num)
    {
        if((num == 1) || (num == 2))
            return 1;
        else
            return fib(num - 1) + fib(num - 2);
    }
}
