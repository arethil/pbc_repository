<?php
    /*
    Задача:
    Даны стороны двух треугольников. Найти
    сумму их периметров и сумму их площадей.
    (Определить процедуру для расчета периметра и
    площади треугольника по его сторонам)
    */
    
    function perimeter($a, $b, $c)
    {
        $P = $a + $b + $c;
        return $P;
    }
    
    function area($a, $b, $c)
    {
        $p = perimeter($a, $b, $c) / 2;
        $S = sqrt($p * ($p - $a) * ($p - $b) * ($p - $c));
        return $S;
    }
    
    $a1 = 7;
    $b1 = 8;
    $c1 = 9;
    
    $a2 = 6;
    $b2 = 4;
    $c2 = 7;
    
    $pSum = perimeter($a1, $b1, $c1) + perimeter($a2, $b2, $c2);
    $aSum = area($a1, $b1, $c1) + area($a2, $b2, $c2);
    
    echo sprintf("Sum of the perimeters of the triangles: %.2f \n", $pSum);
    echo sprintf("Sum of the areas of the triangles: %.2f \n", $aSum);
?>
