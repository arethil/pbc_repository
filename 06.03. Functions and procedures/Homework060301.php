<?php
    // Составить процедуру, "рисующую" на экране горизонтальную линию из любого числа символов "*".
    /*
    $in = fopen('php://stdin', "r");

    fscanf($in, "%d", $n);
    
    printStars($n);
    
    function printStars($n)
    {
        if($n <= 0)
            echo "Incorrect value \"n\"";
        else
            for($i = 1; $i <= $n; $i++)
                echo "* ";
    }
    
    fclose($in);
    */
    
    /*
    Напечатать числа в виде следующей таблицы:
    1 2 ... 10
    1 2 ... 10
    1 2 ... 10
    1 2 ... 10
    */
    
    function printTable()
    {
        for($i = 1; $i <= 4; $i++)
        {
            for($j = 1; $j <= 10; $j++)
                echo $j . " ";
            
            echo "\n";
        }
    }
    
    printTable();
?>
