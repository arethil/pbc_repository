import java.util.Scanner;

public class Homework060301
{
    public static void main(String []args)
    {
        // Составить процедуру, "рисующую" на экране горизонтальную линию из 10 символов "*".
        
        // printStars();
        
        // Написать рекурсивную функцию для вычисления факториала натурального числа n.
        
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        
        if(n < 0)
            System.out.println("Incorrect value \"n\"");
        else
            System.out.println(factorial(n));
        
        in.close();
    }
    
    public static void printStars()
    {
        for(int i = 1; i <= 10; i++)
            System.out.print("* ");
    }
    
    public static long factorial(int n)
    {
        if(n == 0)
            return 1;
        else
            return n * factorial(n - 1);
    }
}
