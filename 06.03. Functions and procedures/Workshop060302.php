<?php
    /*
    Задача:
    Напечатать числа в виде следующей таблицы:
    5
    5 5
    5 5 5
    5 5 5 5
    5 5 5 5 5
    */
    
    function printNum($num)
    {
        echo $num . " ";
    }
    
    function printRow($num, $e)
    {
        for($i = 1; $i <= $e; $i++)
            printNum($num);
        
        echo "\n";
    }
    
    function printTable($num, $rows)
    {
        for($i = 1; $i <= $rows; $i++)
            printRow($num, $i);
    }
    
    printTable(5, 5);
?>
