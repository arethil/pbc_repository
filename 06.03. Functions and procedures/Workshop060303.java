public class Workshop060303
{
    public static void main(String []args)
    {
        /*
        Задача:
        Напечатать полную таблицу сложения в виде:
        1 + 1 = 2   1 + 2 = 3   ...   1 + 9 = 10
        2 + 1 = 3   2 + 2 = 4   ...   2 + 9 = 11
        ...
        9 + 1 = 10  9 + 2 = 11  ...   9 + 9 = 18
        */
        
        printTable();
    }
    
    public static void printSum(int a, int b)
    {
        System.out.printf("%d + %d = %d \t", a, b, a + b);
    }
    
    public static void printRow(int a)
    {
        for(int i = 1; i <= 9; i++)
            printSum(a, i);
        
        System.out.println();
    }
    
    public static void printTable()
    {
        for(int i = 1; i <= 9; i++)
            printRow(i);
    }
}
