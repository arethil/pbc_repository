import java.util.Random;

public class Classwork060401
{
    public static void main(String []args)
    {
        int[] nums1;
        int nums2[];
        
        nums1 = new int[3];
        nums2 = new int[4];
        
        int[] nums3 = new int[5];
        int[] nums4 = {1, 2, 3, 4, 5, 6};
        
        nums1[0] = 1;
        nums1[1] = 2;
        nums1[2] = 4;
        // nums1[3] = 8;
        
        for(int i = 0; i < nums1.length; i++)
            System.out.print(nums1[i] + " ");

        System.out.println();
        
        Random random = new Random();

        int n = random.nextInt(100);
        System.out.println(n);

        int[][] nums5 = new int[2][3];
        int[][] nums6 = new int[][]
        {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        
        for(int i = 0; i < nums6.length; i++)
        {
            for(int j = 0; j < nums6[i].length; j++)
                System.out.print(nums6[i][j] + "\t");
            
            System.out.println();
        }
    }
}
