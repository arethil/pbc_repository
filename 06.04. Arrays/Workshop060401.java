import java.util.Scanner;

public class Workshop060401
{
    public static void main(String []args)
    {
        /*
        Задача:
        Заполнить массив из десяти элементов значениями,
        вводимыми с клавиатуры в ходе выполнения программы.
        */
        
        int[] nums = new int[10];
        Scanner in = new Scanner(System.in);
        
        for(int i = 0; i < nums.length; i++)
            nums[i] = in.nextInt();
        
        for(int i = 0; i < nums.length; i++)
            System.out.print(nums[i] + " ");
        
        in.close();
    }
}
