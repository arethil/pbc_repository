<?php
    /*
    Задача:
    Дан двумерный массив.
    а) Вывести на экран элемент, расположенный в
    правом верхнем углу массива;
    б) Вывести на экран элемент, расположенный в
    левом нижнем углу массива.
    */

    function printMultArray($nums)
    {
        for($i = 0; $i < count($nums); $i++)
        {
            for($j = 0; $j < count($nums[$i]); $j++)
                echo sprintf("%d\t", $nums[$i][$j]);

            echo "\n";
        }

        echo "\n";
    }

    $rows = 5;
    $cols = 6;
    
    for($i = 0; $i < $rows; $i++)
        for($j = 0; $j < $cols; $j++)
            $nums[$i][$j] = random_int(-50, 200);
    
    printMultArray($nums);
        
    echo sprintf("[0][%d] = %d \n", $cols - 1, $nums[0][$cols - 1]);
    echo sprintf("[%d][0] = %d \n", $rows - 1, $nums[$rows - 1][0]);
?>
