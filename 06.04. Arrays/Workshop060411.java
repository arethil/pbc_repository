import java.util.Random;

public class Workshop060411
{
    public static void main(String []args)
    {
        /*
        Задача:
        Дан двумерный массив.
        а) Поменять местами первую и третью строки;
        б) Поменять местами второй и последний столбцы.
        */
        
        int rows = 3;
        int cols = 4;
        
        int[][] nums = new int[rows][cols];
        Random random = new Random();
        
        for(int i = 0; i < nums.length; i++)
            for(int j = 0; j < nums[i].length; j++)
                nums[i][j] = random.nextInt(200);
        
        printMultArray(nums);

        int temp;
        
        for(int i = 0; i < cols; i++)
        {
            temp = nums[0][i];
            nums[0][i] = nums[2][i];
            nums[2][i] = temp;
        }
        
        printMultArray(nums);
        
        for(int i = 0; i < rows; i++)
        {
            temp = nums[i][1];
            nums[i][1] = nums[i][cols - 1];
            nums[i][cols - 1] = temp;
        }
        
        printMultArray(nums);
    }
    
    public static void printMultArray(int[][] nums)
    {
        for(int i = 0; i < nums.length; i++)
        {
            for(int j = 0; j < nums[i].length; j++)
                System.out.print(nums[i][j] + "\t");

            System.out.println();
        }
        
        System.out.println();
    }
}
