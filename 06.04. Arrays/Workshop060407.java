import java.util.Random;

public class Workshop060407
{
    public static void main(String []args)
    {
        /*
        Задача:
        Дан массив. Поменять местами:
        а) второй и пятый элементы;
        б) третий и максимальный элементы.
        Если элементов с максимальным значением
        несколько, то в обмене должен участвовать
        первый из них.
        */
        
        int[] nums = new int[10];
        Random random = new Random();
        
        for(int i = 0; i < nums.length; i++)
            nums[i] = random.nextInt(100);
        
        printArray(nums);
        
        int temp = nums[1];
        nums[1] = nums[4];
        nums[4] = temp;
        
        printArray(nums);
        
        int iMax = indexOfMaxElemOfArray(nums);
        temp = nums[2];
        nums[2] = nums[iMax];
        nums[iMax] = temp;
        
        printArray(nums);
    }
    
    public static void printArray(int[] nums)
    {
        for(int i = 0; i < nums.length; i++)
            System.out.print(nums[i] + " ");
        
        System.out.println();
    }
    
    public static int maxElemOfArray(int[] nums)
    {
        int max = nums[0];
        
        for(int i = 1; i < nums.length; i++)
            if(nums[i] > max)
                max = nums[i];
        
        return max;
    }
    
    public static int indexOfMaxElemOfArray(int[] nums)
    {
        int max = maxElemOfArray(nums);
        int index = 0;
        
        for(int i = 0; i < nums.length; i++)
        {
            if(nums[i] == max)
            {
                index = i;
                break;
            }
        }
        
        return index;
    }
}
