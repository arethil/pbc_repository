<?php
    // Дан массив. Вывести на экран сначала его неотрицательные элементы, затем отрицательные.
    
    for($i = 0; $i < 10; $i++)
        $nums[$i] = random_int(-10, 10);

    printArray($nums);
    printNonNegNumOfArray($nums);
    printNegNumOfArray($nums);
    
    /*
    Дан массив. Поменять местами: первый и минимальный элементы.
    Если элементов с минимальным значением несколько,
    то в обмене должен участвовать последний из них.
    */
    
    echo "Minimum element of the array: " . minElemOfArray($nums), "\n";
    echo "Index of last minimum element of the array: " . indexOfMinElemOfArray($nums), "\n";
    
    $temp = $nums[0];
    $nums[0] = $nums[indexOfMinElemOfArray($nums)];
    $nums[indexOfMinElemOfArray($nums)] = $temp;
    
    printArray($nums);
    
    function printArray($nums)
    {
        for($i = 0; $i < count($nums); $i++)
            echo $nums[$i] . " ";

        echo "\n";
    }
    
    function printNonNegNumOfArray($nums)
    {
        for($i = 0; $i < count($nums); $i++)
            if($nums[$i] >= 0)
                echo $nums[$i] . " ";

        echo "\n";
    }
    
    function printNegNumOfArray($nums)
    {
        for($i = 0; $i < count($nums); $i++)
            if($nums[$i] < 0)
                echo $nums[$i] . " ";

        echo "\n";
    }
    
    function minElemOfArray($nums)
    {
        $min = $nums[0];

        for($i = 1; $i < count($nums); $i++)
            if($nums[$i] < $min)
                $min = $nums[$i];

        return $min;
    }
    
    function indexOfMinElemOfArray($nums)
    {
        $min = minElemOfArray($nums);

        for($i = 0; $i < count($nums); $i++)
            if($nums[$i] == $min)
                $index = $i;

        return $index;
    }
?>
