<?php
    /*
    Задача:
    Дан массив. Все его элементы:
    а) увеличить в 2 раза;
    б) уменьшить на число "a";
    в) разделить на первый элемент.
    */
    
    for($i = 0; $i < 10; $i++)
        $nums[$i] = random_int(0, 100);
    
    print_r($nums);
    
    for($j = 0; $j < count($nums); $j++)
        $nums[$j] *= 2;
    
    print_r($nums);
    
    $in = fopen('php://stdin', "r");

    fscanf($in, "%d", $a);
    
    for($k = 0; $k < count($nums); $k++)
        $nums[$k] -= $a;
    
    fclose($in);

    print_r($nums);
    
    $temp = $nums[0];
    
    for($l = 0; $l < count($nums); $l++)
        $nums[$l] /= $temp;
    
    print_r($nums);
?>
