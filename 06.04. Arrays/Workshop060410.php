<?php
    /*
    Задача:
    Дан двумерный массив. Определить:
    а) сумму всех элементов третьей строки массива;
    б) сумму всех элементов второго столбца массива;
    */
    
    function printMultArray($nums)
    {
        for($i = 0; $i < count($nums); $i++)
        {
            for($j = 0; $j < count($nums[$i]); $j++)
                echo sprintf("%d\t", $nums[$i][$j]);

            echo "\n";
        }

        echo "\n";
    }

    $rows = 5;
    $cols = 6;
    
    for($i = 0; $i < $rows; $i++)
        for($j = 0; $j < $cols; $j++)
            $nums[$i][$j] = random_int(-50, 200);
    
    printMultArray($nums);
    
    $sum1 = 0;
    $sum2 = 0;
    
    for($i = 0; $i < count($nums[2]); $i++)
        $sum1 += $nums[2][$i];
    
    for($j = 0; $j < count($nums); $j++)
        $sum2 += $nums[$j][1];
        
    echo sprintf("The sum of the elements of the 3-rd row: %d \n", $sum1);
    echo sprintf("The sum of the elements of the 2-nd column: %d \n", $sum2);
?>
