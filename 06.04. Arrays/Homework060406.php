<?php
    /*
    Составить программу:
    а) нахождения минимального значения среди элементов любой строки двумерного массива;
    б) нахождения максимального значения среди элементов любого столбца двумерного массива.
    */
    
    for($i = 0; $i < 4; $i++)
        for($j = 0; $j < 6; $j++)
            $nums[$i][$j] = random_int(-50, 200);

    printMultArray($nums);
    
    $m = 3;
    $n = 4;

    echo sprintf("Minimum element of %d row: %d \n", $m, minOfRowElements($nums, $m - 1));
    echo sprintf("Maximum element of %d column: %d \n", $n, maxOfColElements($nums, $n - 1));
    
    function printMultArray($nums)
    {
        for($i = 0; $i < count($nums); $i++)
        {
            for($j = 0; $j < count($nums[$i]); $j++)
                echo $nums[$i][$j], "\t";

            echo "\n";
        }

        echo "\n";
    }
    
    function minOfRowElements($nums, $row)
    {
        $min = $nums[$row][0];

        for($i = 1; $i < count($nums[$row]); $i++)
            if($nums[$row][$i] < $min)
                $min = $nums[$row][$i];

        return $min;
    }
    
    function maxOfColElements($nums, $col)
    {
        $max = $nums[0][$col];

        for($i = 1; $i < count($nums); $i++)
            if($nums[$i][$col] > $max)
                $max = $nums[$i][$col];
        
        return $max;
    }
?>
