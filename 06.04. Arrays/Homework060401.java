public class Homework060401
{
    public static void main(String []args)
    {
        /*
        Заполнить массив из восьми элементов следующими значениями:
        первый элемент массива равен 37, второй 0, третий 50,
        четвертый 46, пятый 34, шестой 46, седьмой 0, восьмой 13.
        */
        
        int[] nums = {37, 0, 50, 46, 34, 46, 0, 13};

        printArray(nums);
        
        // Вывести элементы массива на экран в обратном порядке.
        
        reversePrintArray(nums);
        
        /*
        Определить:
        а) сумму всех элементов массива;
        б) произведение всех элементов массива;
        в) сумму квадратов всех элементов массива;
        г) сумму шести первых элементов массива;
        */
        
        System.out.println("The sum of the array elements: " + arraySum(nums));
        System.out.println("The product of the array elements: " + arrayProduct(nums));
        System.out.println("The sum of the squares of the array elements: " + arraySquareSum(nums));
        System.out.println("The sum of the first 6 array elements: " + arraySumOf6(nums));
    }
    
    public static void printArray(int[] nums)
    {
        for(int i = 0; i < nums.length; i++)
            System.out.print(nums[i] + " ");

        System.out.println();
    }
    
    public static void reversePrintArray(int[] nums)
    {
        for(int i = nums.length - 1; i >= 0; i--)
            System.out.print(nums[i] + " ");

        System.out.println();
    }
    
    public static int arraySum(int[] nums)
    {
        int sum = 0;

        for(int i = 0; i < nums.length; i++)
            sum += nums[i];

        return sum;
    }
    
    public static int arrayProduct(int[] nums)
    {
        int product = 1;

        for(int i = 0; i < nums.length; i++)
            product *= nums[i];

        return product;
    }
    
    public static int arraySquareSum(int[] nums)
    {
        int sum = 0;

        for(int i = 0; i < nums.length; i++)
            sum += Math.pow(nums[i], 2);

        return sum;
    }
    
    public static int arraySumOf6(int[] nums)
    {
        int sum = 0;

        for(int i = 0; i < nums.length; i++)
        {
            sum += nums[i];

            if(i > 5)
                break;
        }
        
        return sum;
    }
}
