import java.util.Random;

public class Homework060405
{
    public static void main(String []args)
    {
        /*
        Дан двумерный массив. Определить:
        а) среднее арифметическое элементов второго столбца массива;
        б) среднее арифметическое элементов n-й строки массива.
        */
        
        int[][] nums = new int[6][4];
        Random random = new Random();

        for(int i = 0; i < nums.length; i++)
            for(int j = 0; j < nums[i].length; j++)
                nums[i][j] = random.nextInt(100);

        printMultArray(nums);
        
        int n = 3;
        
        System.out.printf("Average of 2 column elements: %.2f \n", avgOfColElements(nums, 1));
        System.out.printf("Average of %d row elements: %.2f \n", n, avgOfRowElements(nums, n - 1));
    }
    
    public static void printMultArray(int[][] nums)
    {
        for(int i = 0; i < nums.length; i++)
        {
            for(int j = 0; j < nums[i].length; j++)
                System.out.print(nums[i][j] + "\t");

            System.out.println();
        }
    }
    
    public static double avgOfColElements(int[][] nums, int col)
    {
        int sum = 0;
        int count = 0;

        for(int i = 0; i < nums.length; i++)
        {
            sum += nums[i][col];
            count++;
        }

        double avg = (double)sum / count;

        return avg;
    }
    
    public static double avgOfRowElements(int[][] nums, int row)
    {
        int sum = 0;
        int count = 0;

        for(int i = 0; i < nums[row].length; i++)
        {
            sum += nums[row][i];
            count++;
        }

        double avg = (double)sum / count;

        return avg;
    }
}
