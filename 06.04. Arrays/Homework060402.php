<?php
    // Заполнить массив из двенадцати элементов 1, 2, ... 12.
    
    for($i = 0; $i < 12; $i++)
        $nums[$i] = $i + 1;

    printArray($nums);
    
    /*
    Дан массив. Все его элементы:
    а) увеличить в 2 раза;
    б) уменьшить на число А;
    в) разделить на первый элемент.
    */
    
    for($i = 0; $i < count($nums); $i++)
        $nums[$i] *= 2;

    printArray($nums);
    
    $a = 9;

    for($i = 0; $i < count($nums); $i++)
        $nums[$i] -= $a;

    printArray($nums);
    
    $e = $nums[0];

    for($i = 0; $i < count($nums); $i++)
        $nums[$i] /= $e;
    
    for($i = 0; $i < count($nums); $i++)
        echo sprintf("%.2f ", $nums[$i]);

    echo "\n";
    
    function printArray($nums)
    {
        for($i = 0; $i < count($nums); $i++)
            echo $nums[$i] . " ";
        
        echo "\n";
    }
?>
