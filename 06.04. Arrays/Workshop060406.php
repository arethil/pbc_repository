<?php
    /*
    Задача:
    Дан массив. Определить:
    а) максимальный элемент;
    б) минимальный элемент;
    в) на сколько максимальный элемент
    больше минимального;
    г) индекс максимального элемента;
    д) индекс минимального элемента.
    */
    
    for($i = 0; $i < 10; $i++)
        $nums[$i] = random_int(-10, 10);
    
    print_r($nums);
    
    $max = maxElemOfArray($nums);
    $min = minElemOfArray($nums);
    $dif = $max - $min;
    $iMax = indexOfMaxElemOfArray($nums);
    $iMin = indexOfMinElemOfArray($nums);
    
    echo sprintf("Maximum element of array: %d \n", $max);
    echo sprintf("Minimum element of array: %d \n", $min);
    echo sprintf("Value difference: %d \n", $dif);
    echo sprintf("Index of maximum element of array: %d \n", $iMax);
    echo sprintf("Index of minimum element of array: %d \n", $iMin);
    
    function maxElemOfArray($nums)
    {
        $max = $nums[0];
        
        for($i = 1; $i < count($nums); $i++)
            if($nums[$i] > $max)
                $max = $nums[$i];
        
        return $max;
    }
    
    function minElemOfArray($nums)
    {
        $min = $nums[0];
        
        for($i = 1; $i < count($nums); $i++)
            if($nums[$i] < $min)
                $min = $nums[$i];
        
        return $min;
    }
    
    function indexOfMaxElemOfArray($nums)
    {
        $max = maxElemOfArray($nums);
        
        for($i = 0; $i < count($nums); $i++)
        {
            if($nums[$i] == $max)
            {
                $index = $i;
                break;
            }
        }
        
        return $index;
    }
    
    function indexOfMinElemOfArray($nums)
    {
        $min = minElemOfArray($nums);
        
        for($i = 0; $i < count($nums); $i++)
        {
            if($nums[$i] == $min)
            {
                $index = $i;
                break;
            }
        }
        
        return $index;
    }
?>
