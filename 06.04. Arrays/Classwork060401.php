<?php
    $nums1[0] = 1;
    $nums1[1] = 2;
    $nums1[2] = 4;
    
    function printArray($nums)
    {
        for($i = 0; $i < count($nums); $i++)
            echo $nums[$i], " ";
        
        echo "\n";
    }
    
    printArray($nums1);
    
    $nums1[] = 8;
    $nums1[] = 16;
    $nums1[] = 32;
    
    printArray($nums1);
    
    $nums2 = array(64, 128, 256, 512);
    
    print_r($nums2);

    $random = random_int(0, 100);
    echo $random, "\n";

    $nums = array(array(1, 2, 3, 4), array(5, 6, 7, 8));
    
    print_r($nums);
    
    for($i = 0; $i < count($nums); $i++)
    {
        for($j = 0; $j < count($nums[$i]); $j++)
            echo $nums[$i][$j], "\t";
        
        echo "\n";
    }
?>
