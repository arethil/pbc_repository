import java.util.Random;
import java.util.Scanner;

public class Workshop060403
{
    public static void main(String []args)
    {
        /*
        Задача:
        Дан массив. Составить программу:
        а) расчета квадратного корня из любого элемента массива;
        б) расчета среднего арифметического двух любых элементов массива.
        
        Предусмотреть возможность ошибочных обращений вне границ массива.
        */
        
        int[] nums = new int[10];
        Random random = new Random();
        Scanner in = new Scanner(System.in);
        
        for(int i = 0; i < nums.length; i++)
            nums[i] = random.nextInt(100);
        
        printArray(nums);
        /*
        int x = in.nextInt();
        
        elemSqrt(nums, x);
        */
        int y = in.nextInt();
        int z = in.nextInt();
        
        elemAvg(nums, y, z);
        
        in.close();
    }
    
    public static void printArray(int[] nums)
    {
        for(int i = 0; i < nums.length; i++)
            System.out.print(nums[i] + " ");
        
        System.out.println();
    }
    
    public static void elemSqrt(int[] nums, int i)
    {
        if((i >= 0) && (i < nums.length))
        {
            double result = Math.sqrt(nums[i]);
            System.out.printf("[%d] = %d \n", i, nums[i]);
            System.out.println("Sqrt = " + result);
        }
        else
        {
            System.out.println("Incorrect value of index");
        }
    }
    
    public static void elemAvg(int[] nums, int i1, int i2)
    {
        if((i1 >= 0) && (i1 < nums.length) && (i2 >= 0) && (i2 < nums.length))
        {
            double result = ((double)nums[i1] + nums[i2]) / 2;
            System.out.printf("[%d] = %d; [%d] = %d \n", i1, nums[i1], i2, nums[i2]);
            System.out.println("Avg = " + result);
        }
        else
        {
            System.out.println("Incorrect value of index");
        }
    }
}
