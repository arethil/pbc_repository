import java.util.Scanner;

public class Workshop060405
{
    public static void main(String []args)
    {
        /*
        Задача:
        Дан массив. Напечатать:
        а) все неотрицательные элементы;
        б) все элементы, не превышающие число 100.
        */
        
        int[] nums = new int[10];
        Scanner in = new Scanner(System.in);
        
        for(int i = 0; i < nums.length; i++)
            nums[i] = in.nextInt();
        
        printArray(nums);
        
        for(int j = 0; j < nums.length; j++)
            if(nums[j] >= 0)
                System.out.print(nums[j] + " ");
        
        System.out.println();
        
        for(int l = 0; l < nums.length; l++)
            if(nums[l] <= 100)
                System.out.print(nums[l] + " ");

        in.close();
    }
    
    public static void printArray(int[] nums)
    {
        for(int i = 0; i < nums.length; i++)
            System.out.print(nums[i] + " ");
        
        System.out.println();
    }
}
