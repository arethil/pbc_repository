import java.util.Random;

public class Homework060403
{
    public static void main(String []args)
    {
        /*
        Дан массив целых чисел. Напечатать:
        а) все четные элементы;
        б) все элементы, оканчивающиеся нулем.
        */
        
        int[] nums = new int[10];
        Random random = new Random();
        
        for(int i = 0; i < nums.length; i++)
            nums[i] = random.nextInt(100);

        printArray(nums);
        printEvenElemOfArray(nums);
        printElemOfArrayEnd0(nums);
        
        /*
        Дан массив. Определить:
        а) количество максимальных элементов в массиве;
        б) количество минимальных элементов в массиве.
        */
        
        System.out.println("Maximum element of the array: " + maxElemOfArray(nums));
        System.out.println("Minimum element of the array: " + minElemOfArray(nums));
        System.out.println("Number of maximum elements of the array: " + countOfMaxElemOfArray(nums));
        System.out.println("Number of minimum elements of the array: " + countOfMinElemOfArray(nums));
    }
    
    public static void printArray(int[] nums)
    {
        for(int i = 0; i < nums.length; i++)
            System.out.print(nums[i] + " ");

        System.out.println();
    }
    
    public static void printEvenElemOfArray(int[] nums)
    {
        for(int i = 0; i < nums.length; i++)
            if((nums[i] % 2) == 0)
                System.out.print(nums[i] + " ");

        System.out.println();
    }
    
    public static void printElemOfArrayEnd0(int[] nums)
    {
        for(int i = 0; i < nums.length; i++)
            if((nums[i] % 10) == 0)
                System.out.print(nums[i] + " ");

        System.out.println();
    }
    
    public static int maxElemOfArray(int[] nums)
    {
        int max = nums[0];

        for(int i = 1; i < nums.length; i++)
            if(nums[i] > max)
                max = nums[i];

        return max;
    }
    
    public static int minElemOfArray(int[] nums)
    {
        int min = nums[0];

        for(int i = 1; i < nums.length; i++)
            if(nums[i] < min)
                min = nums[i];

        return min;
    }
    
    public static int countOfMaxElemOfArray(int[] nums)
    {
        int max = maxElemOfArray(nums);
        int count = 0;

        for(int i = 0; i < nums.length; i++)
            if(nums[i] == max)
                count++;

        return count;
    }
    
    public static int countOfMinElemOfArray(int[] nums)
    {
        int min = minElemOfArray(nums);
        int count = 0;

        for(int i = 0; i < nums.length; i++)
            if(nums[i] == min)
                count++;
        
        return count;
    }
}
