import java.util.Scanner;

public class Workshop060409
{
    public static void main(String []args)
    {
        /*
        Задача:
        Дан двумерный массив из m строк и n столбцов.
        Заполнить его значениями, вводимыми с консоли.
        Заполнение проводить по строкам, начиная с первой
        (а в ней начиная с первого элемента).
        */        
        
        int[][] nums = new int[2][3];
        Scanner in = new Scanner(System.in);
        
        for(int i = 0; i < nums.length; i++)
            for(int j = 0; j < nums[i].length; j++)
                nums[i][j] = in.nextInt();
        
        printMultArray(nums);
        
        in.close();
    }
    
    public static void printMultArray(int[][] nums)
    {
        for(int i = 0; i < nums.length; i++)
        {
            for(int j = 0; j < nums[i].length; j++)
                System.out.print(nums[i][j] + "\t");

            System.out.println();
        }
        
        System.out.println();
    }
}
