public class Workshop060103
{
    public static void main(String []args)
    {
        /*
        Задача:
        Записать логические выражения,
        которые имеют значение "истина" только
        при выполнении указанных условий:
        а) х > 2 и у > 3;
        б) неверно, что х > 2.
        */
        
        int x = 5;
        int y = 7;

        boolean b1 = (x > 2) && (y > 3);
        boolean b2 = (x > 2) || (y > 3);
        
        System.out.println(b1);
        System.out.println(b2);
        
        x = 1;
        
        boolean b3 = !((x > 2) && (y > 3));
        boolean b4 = (x > 2) || (y > 3);
        boolean b5 = (x > 2) ^ (y > 3);
        
        System.out.println(b3);
        System.out.println(b4);
        System.out.println(b5);
    }
}
