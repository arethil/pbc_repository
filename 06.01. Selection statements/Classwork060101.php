<?php
    $hi = fopen('php://stdin', "r");
    $ho = fopen('php://stdout', "w");
    
    fscanf($hi, "%d", $num);
    fwrite($ho, sprintf("Number: %d \n", $num));
    
    fclose($hi);
    fclose($ho);
    
    $a = 10;
    $aa = "10a";
    $b = 4;
    
    $c1 = $a == $b;
    echo var_export($c1), "\n";
    
    $c2 = $a == 10;
    echo var_export($c2), "\n";
    
    $c3 = $aa == 10;
    echo var_export($c3), "\n";
    
    $c4 = $aa === 10;
    echo var_export($c4), "\n";
    
    $c5 = $a != $b;
    echo var_export($c5), "\n";
    
    $c6 = $a != 10;
    echo var_export($c6), "\n";
    
    $c7 = $aa != 10;
    echo var_export($c7), "\n";
    
    $c8 = $aa !== 10;
    echo var_export($c8), "\n";
    
    $c9 = $a < $b;
    echo var_export($c9), "\n";
    
    $c10 = $a > $b;
    echo var_export($c10), "\n";
    
    $c11 = $a <= $aa;
    echo var_export($c11), "\n";
    
    $c12 = $a >= 10;
    echo var_export($c12), "\n";
    
    echo var_export(!$c12), "\n";
    
    $d1 = (5 < 6) || (4 > 7);
    echo var_export($d1), "\n";
    
    $d2 = (5 > 6) || (4 > 7);
    echo var_export($d2), "\n";
    
    $d3 = (5 < 6) && (4 > 7);
    echo var_export($d3), "\n";
    
    $d4 = (5 < 6) && (4 < 7);
    echo var_export($d4), "\n";
    
    $d5 = (5 < 6) ^ (4 > 7);
    echo var_export($d5), "\n";
    
    $d6 = (5 < 6) ^ (4 < 7);
    echo var_export($d6), "\n";
    
    $num1 = 7;
    $num2 = 31;
    
    if($num1 > $num2)
    {
        echo "num1 more than num2 \n";
    }
    else
    {
        echo "num1 less than num2 \n";
    }
    
    $num3 = 3;

    switch($num3)
    {
        case 1:
            echo "num3 is 1 \n";
            break;
        case 2:
            echo "num3 is 2 \n";
            break;
        case 3:
            echo "num3 is 3 \n";
            break;
        default:
            echo "num3 is not 1, 2 or 3 \n";
    }
?>
