<?php
    /*
    Задача:
    Дано трёхзначное число. Найти:
    а) число единиц в нём;
    б) число десятков в нём;
    в) сумму его цифр;
    г) произведение его цифр.
    */
    
    $in = fopen('php://stdin', "r");
    $out = fopen('php://stdout', "w");
    
    fscanf($in, "%d", $num);
    fwrite($out, sprintf("Number is %d \n", $num));
    
    $u = $num % 10;
    $t = ($num % 100) / 10;
    settype($t, "integer");
    $h = $num / 100;
    settype($h, "integer");

    $sum = $u + $t + $h;
    $pro = $u * $t * $h;
    
    echo sprintf("Units: %d\nTens: %d\nSum: %d\nProduct: %d\n", $u, $t, $sum, $pro);
    
    fclose($in);
    fclose($out);
?>
