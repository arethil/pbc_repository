import java.util.Scanner;

public class Workshop060111
{
    public static void main(String []args)
    {
        /*
        Задача:
        В чемпионате по футболу команде за выигрыш дается 3 очка,
        за проигрыш 0, за ничью 1. Известно количество очков, полученных
        командой за игру. Определить словесный результат
        игры (выигрыш, проигрыш или ничья).
        */

        Scanner in = new Scanner(System.in);
        
        int result = in.nextInt();
        
        switch(result)
        {
            case 0:
                System.out.println("Loss");
                break;
            case 1:
                System.out.println("Draw");
                break;
            case 3:
                System.out.println("Win");
                break;
            default:
                System.out.println("Incorrect number of points");
        }
        
        in.close();
    }
}
