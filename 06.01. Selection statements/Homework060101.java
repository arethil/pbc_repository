import java.util.Scanner;

public class Homework060101
{
    public static void main(String []args)
    {
        /*
        С начала суток прошло n секунд. Определить:
        а) сколько полных часов прошло с начала суток;
        б) сколько полных минут прошло с начала очередного часа;
        в) сколько полных секунд прошло с начала очередной минуты.
        */
        /*
        Scanner in = new Scanner(System.in);

        int sec = in.nextInt();
        System.out.println("Seconds have passed since the beginning of the day: " + sec);
        
        int h = sec / 3600;
        int m = (sec % 3600) / 60;
        int s = (sec % 3600) % 60;
        
        System.out.println("a: " + h);
        System.out.println("b: " + m);
        System.out.println("c: " + s);
        
        in.close();
        */

        // Даны радиус круга и сторона квадрата. У какой фигуры площадь больше?
        /*
        Scanner in = new Scanner(System.in);

        int r = in.nextInt();
        int a = in.nextInt();
        
        double cS = Math.PI * Math.pow(r, 2);
        double sS = Math.pow(a, 2);
        
        if(cS > sS)
        {
            System.out.println("Circle area is greater");
        }
        else
        {
            if(cS < sS)
            {
                System.out.println("Square area is greater");
            }
            else
            {
                System.out.println("Areas is equal");
            }
        }

        in.close();
        */

        /*
        Записать условие, которое является истинным, когда:
        а) каждое из чисел А и В больше 100;
        б) только одно из чисел А и В четное.
        */
        /*
        Scanner in = new Scanner(System.in);

        int a = in.nextInt();
        int b = in.nextInt();
        
        boolean c1 = (a > 100) && (b > 100);
        System.out.println(c1);

        boolean c2 = ((a % 2) == 0) ^ ((b % 2) == 0);
        System.out.println(c2);

        in.close();
        */

        /*
        Даны три вещественных числа a, b, c. Проверить:
        а) выполняется ли неравенство a < b < c;
        б) выполняется ли неравенство b > a > c.
        */
        /*
        Scanner in = new Scanner(System.in);
        
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        
        boolean result1 = (a < b) && (b < c);
        System.out.println(result1);
        
        boolean result2 = (b > a) && (a > c);
        System.out.println(result2);
        
        in.close();
        */
        
        // Составить программу нахождения произведения двух наименьших из трех различных чисел.
        
        Scanner in = new Scanner(System.in);
        
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        int result;
        
        if((a >= b) && (a >= c))
        {
            result = b * c;
        }
        else
        {
            if((b >= a) && (b >= c))
            {
                result = a * c;
            }
            else
            {
                result = a * b;
            }
        }
        
        System.out.println(result);
        
        in.close();
    }
}
