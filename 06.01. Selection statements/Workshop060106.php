<?php
    /*
    Задача:
    Записать условие, которое является истинным, когда:
    а) целое N кратно пяти или семи;
    б) целое N кратно четырем и не оканчивается нулем.
    */
    
    $in = fopen('php://stdin', "r");
    $out = fopen('php://stdout', "w");
    
    fscanf($in, "%d", $num);
    fwrite($out, sprintf("Number is %d \n", $num));
    
    $b1 = (($num % 5 == 0) || ($num % 7 == 0));
    echo "Number is a multiple of 5 or 7? \n";
    echo var_export($b1), "\n";
    
    $b2 = (($num % 4 == 0) && ($num % 10 != 0));
    echo "Number is a multiple of 4 and does not end in 0? \n";
    echo var_export($b2), "\n";
    
    fclose($in);
    fclose($out);
?>
