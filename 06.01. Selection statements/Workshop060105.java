import java.util.Scanner;

public class Workshop060105
{
    public static void main(String []args)
    {
        /*
        Задача:
        Дано натуральное число. Определить:
        а) является ли оно четным?
        б) оканчивается ли оно цифрой 7?
        */
        
        Scanner in = new Scanner(System.in);
        
        int num = in.nextInt();
        
        boolean b1 = (num % 2) == 0;
        System.out.println("Number is even: " + b1);
        
        boolean b2 = (num % 10) == 7;
        System.out.println("Number ends with 7: " + b2);
        
        in.close();
    }
}
