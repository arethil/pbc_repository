import java.util.Scanner;

public class Classwork060101
{
    public static void main(String []args)
    {
        Scanner in = new Scanner(System.in);

        int num = in.nextInt();
        System.out.printf("Number: %d \n", num);

        in.close();
        
        int a = 10;
        int b = 4;
        
        boolean c1 = a == b;
        System.out.println(c1);
        
        boolean c2 = a == 10;
        System.out.println(c2);
        
        boolean c3 = a != b;
        System.out.println(c3);
        
        boolean c4 = a != 10;
        System.out.println(c4);
        
        boolean c5 = a > b;
        System.out.println(c5);
        
        boolean c6 = a < 10;
        System.out.println(c6);
        
        boolean c7 = a >= b;
        System.out.println(c7);
        
        boolean c8 = a <= 10;
        System.out.println(c8);
        
        System.out.println(!c8);
        
        boolean d1 = (5 < 6) || (4 > 7);
        System.out.println(d1);
        
        boolean d2 = (5 > 6) || (4 > 7);
        System.out.println(d2);
        
        boolean d3 = (5 < 6) && (4 > 7);
        System.out.println(d3);
        
        boolean d4 = (5 < 6) && (4 < 7);
        System.out.println(d4);
        
        boolean d5 = (5 < 6) ^ (4 < 7);
        System.out.println(d5);
        
        boolean d6 = (5 < 6) ^ (4 > 7);
        System.out.println(d6);
        
        int num1 = 7;
        int num2 = 13;
        
        if(num1 > num2)
        {
            System.out.println("num1 more than num2");
        }
        else
        {
            System.out.println("num1 less than num2");
        }
        
        int num3 = 3;

        switch(num3)
        {
            case 1:
                System.out.println("num3 is 1");
                break;
            case 2:
                System.out.println("num3 is 2");
                break;
            case 3:
                System.out.println("num3 is 3");
                break;
            default:
                System.out.println("num3 is not 1, 2 or 3");
        }
    }
}
