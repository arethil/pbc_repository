import java.util.Scanner;

public class Workshop060101
{
    public static void main(String []args)
    {
        /*
        Задача:
        Даны два целых числа. Найти:
        а) их среднее арифметическое;
        б) их среднее геометрическое.
        */
        
        Scanner in = new Scanner(System.in);
        
        int num1 = in.nextInt();
        int num2 = in.nextInt();
        
        double aAvg = (double)(num1 + num2) / 2;
        double gAvg = Math.sqrt(num1 * num2);

        System.out.println("Arithmetic average: " + aAvg);
        System.out.println("Geometric average: " + gAvg);
        
        in.close();
    }
}
