<?php
    /*
    Задача:
    Известны площади круга и квадрата. Определить:
    а) уместится ли круг в квадрате?
    б) уместится ли квадрат в круге?
    */
    
    $in = fopen('php://stdin', "r");
    
    fscanf($in, "%d", $cS);
    fscanf($in, "%d", $sS);
    
    $r = sqrt($cS / pi());
    echo sprintf("Circle's radius is %.4f \n", $r);

    $a = sqrt($sS);
    echo sprintf("Square side is %.4f \n", $a);
    
    $b1 = $r <= ($a / 2);
    echo "Can a circle fit in a square? \n";
    echo var_export($b1), "\n";
    
    $b2 = $r >= ($a / sqrt(2));
    echo "Can a square fit in a circle? \n";
    echo var_export($b2), "\n";
    
    fclose($in);
?>
