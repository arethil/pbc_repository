import java.util.Scanner;

public class Workshop060107
{
    public static void main(String []args)
    {
        /*
        Задача:
        Дано трёхзначное число. Определить:
        а) входит ли в него цифра 6?
        б) входит ли в него цифра n?
        */
        
        Scanner in = new Scanner(System.in);
        
        int num = in.nextInt();
        int n = in.nextInt();
        
        int h = num / 100;
        int t = (num % 100) / 10;
        int u = num % 10;
        
        boolean b1 = (h == 6) || (t == 6) || (u == 6);
        System.out.println(b1);
        
        boolean b2 = (h == n) || (t == n) || (u == n);
        System.out.println(b2);
        
        in.close();
    }
}
