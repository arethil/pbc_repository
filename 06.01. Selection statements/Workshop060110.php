<?php
    /*
    Задача:
    Составить программу, которая в зависимости
    от порядкового номера дня недели
    выводит на экран его название.
    */

    $in = fopen('php://stdin', "r");
    
    fscanf($in, "%d", $num);
    
    switch($num)
    {
        case 1:
            echo "Monday \n";
            break;
        case 2:
            echo "Tuesday \n";
            break;
        case 3:
            echo "Wednesday \n";
            break;
        case 4:
            echo "Thursday \n";
            break;
        case 5:
            echo "Friday \n";
            break;
        case 6:
            echo "Saturday \n";
            break;
        case 7:
            echo "Sunday \n";
            break;
        default:
            echo "Incorrect number";
    }
    
    fclose($in);
?>
