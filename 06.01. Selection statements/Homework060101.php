<?php
    /*
    Два автомобиля едут навстречу друг другу с постоянными скоростями V1 и V2 км/час.
    Определить, через какое время автомобили встретятся, если расстояние между ними было S км.
    */
    /*
    $in = fopen('php://stdin', "r");
    $out = fopen('php://stdout', "w");
    
    fscanf($in, "%d", $S);
    fscanf($in, "%d", $v1);
    fscanf($in, "%d", $v2);
    
    $v = $v1 + $v2;
    $t = $S / $v;
    
    fwrite($out, sprintf("Desired time: %.2f \n", $t));
    
    fclose($in);
    fclose($out);
    */

    /*
    Вычислить значение логического выражения
    при следующих значениях логических величин А, В и С:
    А = Истина, В = Ложь, С = Ложь:
    а) А или В;
    б) А и В;
    в) В или С.
    */
    /*
    $a = true;
    $b = false;
    $c = false;
    
    echo var_export($a || $b), "\n"; // true
    echo var_export($a && $b), "\n"; // false
    echo var_export($b || $c), "\n";  // false
    */

    /*
    Дано двузначное число. Определить:
    а) является ли сумма его цифр двузначным числом;
    б) больше ли числа а сумма его цифр.
    */
    /*
    $in = fopen('php://stdin', "r");

    fscanf($in, "%d", $num);
    fscanf($in, "%d", $a);
    
    $u = $num % 10;
    $t = $num / 10;
    settype($t, "integer");
    
    $sum = $u + $t;
    echo $sum, "\n";
    
    $b1 = $sum >= 10;
    echo var_export($b1), "\n";
    
    $b2 = $sum > $a;
    echo var_export($b2), "\n";
    
    fclose($in);
    */

    /*
    Даны два числа. Если квадратный корень из второго числа меньше первого числа,
    то увеличить второе число в пять раз.
    */
    $in = fopen('php://stdin', "r");
    
    fscanf($in, "%d", $num1);
    fscanf($in, "%d", $num2);
    
    if($num1 > sqrt($num2))
    {
        $num2 *= 5;
    }
    
    echo sprintf("%d\n%d\n", $num1, $num2);
    
    fclose($in);
?>
