import java.util.Scanner;

public class Workshop060109
{
    public static void main(String []args)
    {
        /*
        Задача:
        Известен вес боксёра любителя. Известно,
        что вес таков, что боксёр может быть отнесён к
        одной из трёх весовых категорий:
        1) лёгкий вес, до 60 кг;
        2) средний вес, до 75 кг;
        3) тяжёлый вес, до 91 кг;
        4) супертяжёлый вес, с 91 кг.
        Определить, в какой категории будет выступать
        данный боксёр.
        */

        Scanner in = new Scanner(System.in);
        
        double w = in.nextInt();
        
        if(w <= 0)
        {
            System.out.println("Incorrect weight");
        }
        else
        {
            if(w >= 91)
            {
                System.out.println("Superheavy weight");
            }
            else
            {
                if(w >= 75)
                {
                    System.out.println("Heavy weight");
                }
                else
                {
                    if(w >= 60)
                    {
                        System.out.println("Medium weight");
                    }
                    else
                    {
                        System.out.println("Light weight");
                    }
                }
            }
        }
        
        in.close();
    }
}
